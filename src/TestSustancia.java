import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;



class TestSustancia {

	Toxicidad t = new Toxicidad();
	Sustancia s = new Sustancia();
	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void test1() {
		s.PH = 7;
		s.NP = "20-30";
		s.concentracion="ALTA";
		s.acidez= "BASICO";
		s.tipo="MTP";		
		s.composicion= "ACTIVO";
		
		assertEquals("VALIDO", t.nivel(s));
	}
	
	@Test
	void test2() {			
		s.PH = 7;
		s.NP = "20-30";
		s.concentracion="BAJA";
		s.acidez= "ALCALINO";
		s.tipo="HOMEOPATICO";		
		s.composicion= "EXCIPIENTE";
		assertEquals("VALIDO", t.nivel(s));
	}
	
	@Test
	void test3() {
		s.PH = 7;
		s.NP = "";
		s.concentracion="MEDIA";
		s.acidez= "";
		s.tipo="";		
		s.composicion= "";
		
		assertEquals("VALIDO", t.nivel(s));
		
	}
	
	
	@Test
	void test4() {
		s.PH = 6;
		s.NP = "";
		s.concentracion="MEDIA";
		s.acidez= "BASICO";
		s.tipo="HOMEOPATICO";		
		s.composicion= "";
		
		assertEquals("VALIDO", t.nivel(s));
	}
	
	
	@Test
	void test5() {
		s.PH = 6;
		s.NP = "";
		s.concentracion="ALTA";
		s.acidez= "ALCALINO";
		s.tipo="";		
		s.composicion= "ACTIVO";
			
		assertEquals("VALIDO", t.nivel(s));
	}
	
	@Test
	void test6() {
		s.PH = 6;
		s.NP = "10-20";
		s.concentracion="BAJA";
		s.acidez= "";
		s.tipo="MTP";		
		s.composicion= "ACTIVO";
			
		assertEquals("VALIDO", t.nivel(s));	
		
	}
	
	@Test
	void test7() {
		s.PH = 0;
		s.NP = "";
		s.concentracion="BAJA";
		s.acidez= "BASICO";
		s.tipo="";		
		s.composicion= "EXIPIENTE";
			
		assertEquals("VALIDO", t.nivel(s));	
		
	}
	
	@Test
	void test8() {
		s.PH = 0;
		s.NP = "";
		s.concentracion="MEDIA";
		s.acidez= "ALCALINO";
		s.tipo="MTP";		
		s.composicion= "";
			
		assertEquals("VALIDO", t.nivel(s));	
		
	}
	
	
	@Test
	void test9() {
		s.PH = 0;
		s.NP = "";
		s.concentracion="ALTA";
		s.acidez= "";
		s.tipo="HOMEOPATICO";		
		s.composicion= "ACTIVO";
			
		assertEquals("VALIDO", t.nivel(s));	
		
	}
	
}
